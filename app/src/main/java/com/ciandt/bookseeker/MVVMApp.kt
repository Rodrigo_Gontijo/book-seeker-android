package com.ciandt.bookseeker

import android.app.Application
import com.ciandt.bookseeker.di.*
import org.koin.android.ext.android.startKoin

@Suppress("Unused")
class CiandTItunes : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(
            networkModule,
            apiModule,
            roomModule,
            viewModelModule
        ))
    }

}