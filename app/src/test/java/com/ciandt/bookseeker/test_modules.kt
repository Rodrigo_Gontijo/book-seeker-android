package com.ciandt.bookseeker

import com.ciandt.bookseeker.data.db.AppDatabase
import com.ciandt.bookseeker.data.remote.api.SearchAPI
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module



val testApiModule = module {
    single { AppDatabase.getInstance(androidApplication()) }
    single(createOnStart = false) { get<AppDatabase>().getBookmarkDao() }
    single { DummySearchAPI() as SearchAPI }
}