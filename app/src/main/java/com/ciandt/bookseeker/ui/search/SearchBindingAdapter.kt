package com.ciandt.bookseeker.ui.search

import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.ui.bookdetails.BookDetailsActivity
import com.squareup.picasso.Picasso


@BindingAdapter(value = ["bookListRepository", "viewModel"])
fun setRepositories(view: RecyclerView, items: List<Book>, vm: SearchViewModel) {
    view.adapter?.run {
        if (this is RepositoryAdapter) {
            this.items = items
            this.notifyDataSetChanged()
        }
    } ?: run {
        RepositoryAdapter(items, vm).apply {
            view.adapter = this
        }
    }
}


@BindingAdapter("app:image")
fun setImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}


@BindingAdapter("app:onClick")
fun setOnClickListener(view: View, book: Book) {
    view.setOnClickListener {
        val intent = Intent(view.context, BookDetailsActivity::class.java)
        intent.putExtra("BookRepository", book)
        view.context.startActivity(intent)
    }
}