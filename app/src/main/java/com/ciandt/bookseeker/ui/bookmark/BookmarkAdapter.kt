package com.ciandt.bookseeker.ui.bookmark

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.ciandt.bookseeker.R
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.databinding.ItemBookmarkBinding
import com.ciandt.bookseeker.ui.BindingViewHolder


class BookmarkAdapter(val vm: BookmarkViewModel) :
    PagedListAdapter<BookDB, BookmarkAdapter.BookmarkViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarkViewHolder {
        return BookmarkViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_bookmark,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BookmarkViewHolder, position: Int) {
        getItem(position)?.run {
            holder.binding.item = this
            holder.binding.vm = vm
        }
    }

    class BookmarkViewHolder(view: View) : BindingViewHolder<ItemBookmarkBinding>(view)

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BookDB>() {
            override fun areItemsTheSame(oldItem: BookDB, newItem: BookDB) = oldItem.artistId == newItem.artistId
            override fun areContentsTheSame(oldItem: BookDB, newItem: BookDB) = oldItem == newItem
        }
    }
}