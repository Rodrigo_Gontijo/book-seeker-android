package com.ciandt.bookseeker.ui.search

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.LayoutRes
import com.ciandt.bookseeker.R
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.databinding.ActivitySearchBinding
import com.ciandt.bookseeker.extension.with
import com.ciandt.bookseeker.ui.BindingActivity
import com.ciandt.bookseeker.ui.bookdetails.BookDetailsActivity
import com.ciandt.bookseeker.ui.bookmark.BookmarkActivity
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.android.setProperty
import org.koin.androidx.viewmodel.ext.android.getViewModel


class SearchActivity : BindingActivity<ActivitySearchBinding>() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_search

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding.vm = getViewModel()
        binding.setLifecycleOwner(this)

    }

    override fun onDestroy(){
        super.onDestroy()
        disposables.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.bookmark -> {
                startActivity(Intent(this, BookmarkActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun startDetailsActivity(book: Book) {
        val intent = Intent(this, BookDetailsActivity::class.java)
        intent.putExtra("BookRepository", book)
        startActivity(intent)
    }
}