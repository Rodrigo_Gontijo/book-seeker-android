package com.ciandt.bookseeker

import android.content.Context
import com.ciandt.bookseeker.ui.bookmark.BookmarkViewModel

import org.mockito.Mockito
import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import androidx.room.Room
import com.ciandt.bookseeker.data.db.AppDatabase
import com.ciandt.bookseeker.data.db.dao.BookmarkDao
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.ui.search.SearchViewModel
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsNull.notNullValue
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.get
import org.koin.test.KoinTest
import org.mockito.Mockito.mock


class BookmarkViewModelTest : KoinTest {
    private lateinit var viewModel: BookmarkViewModel
    lateinit var instrumentationContext: Context
    private lateinit var userDao: BookmarkDao
    private lateinit var db: AppDatabase

    @Before fun before() {
        startKoin(listOf(testApiModule))
        instrumentationContext = mock(Context::class.java)

        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
            override fun isMainThread(): Boolean  = true
            override fun postToMainThread(runnable: Runnable) = runnable.run()
        })

        db = Room.inMemoryDatabaseBuilder(
            instrumentationContext, AppDatabase::class.java).build()
        userDao = db.getBookmarkDao()

        viewModel = BookmarkViewModel(userDao)
    }

    @After fun after() {
        ArchTaskExecutor.getInstance().setDelegate(null)
        stopKoin()
    }

    @Test fun viewModelNotNull() {
        assertThat(viewModel, notNullValue())

    }
}