package com.ciandt.bookseeker.data.db.dao

import androidx.paging.DataSource
import androidx.room.*
import com.ciandt.bookseeker.data.db.entity.BookDB


@Dao
interface BookmarkDao {

    @Query("SELECT * FROM BookDB ORDER BY created ASC")
    fun findAll(): DataSource.Factory<Int, BookDB>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(book: BookDB)

    @Delete
    fun delete(book: BookDB)

}