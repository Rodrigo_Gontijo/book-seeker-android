package com.ciandt.bookseeker.ui.bookmark

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.ciandt.bookseeker.R
import com.ciandt.bookseeker.databinding.ActivityBookmarkBinding
import com.ciandt.bookseeker.ui.BindingActivity
import org.koin.androidx.viewmodel.ext.android.getViewModel


class BookmarkActivity : BindingActivity<ActivityBookmarkBinding>() {
    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_bookmark

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.vm = getViewModel()
        binding.setLifecycleOwner(this)
    }
}