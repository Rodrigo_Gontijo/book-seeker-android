package com.ciandt.bookseeker.data.remote.domain

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Book(
    @SerializedName("artistId")
    val artistId: Int,
    @SerializedName("artistIds")
    val artistIds: List<Int>?,
    @SerializedName("artistName")
    val artistName: String?,
    @SerializedName("artistViewUrl")
    val artistViewUrl: String?,
    @SerializedName("artworkUrl100")
    val artworkUrl100: String?,
    @SerializedName("artworkUrl60")
    val artworkUrl60: String?,
    @SerializedName("averageUserRating")
    val averageUserRating: Double?,
    @SerializedName("currency")
    val currency: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("fileSizeBytes")
    val fileSizeBytes: Int?,
    @SerializedName("formattedPrice")
    val formattedPrice: String?,
    @SerializedName("genreIds")
    val genreIds: List<String>?,
    @SerializedName("genres")
    val genres: List<String>?,
    @SerializedName("kind")
    val kind: String?,
    @SerializedName("price")
    val price: Double?,
    @SerializedName("releaseDate")
    val releaseDate: String?,
    @SerializedName("trackCensoredName")
    val trackCensoredName: String?,
    @SerializedName("trackId")
    val trackId: Int?,
    @SerializedName("trackName")
    val trackName: String?,
    @SerializedName("trackViewUrl")
    val trackViewUrl: String?,
    @SerializedName("userRatingCount")
    val userRatingCount: Int?
) : Parcelable