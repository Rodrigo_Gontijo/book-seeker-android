package com.ciandt.bookseeker.data.db.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.ciandt.bookseeker.data.db.converter.DateConverter
import com.ciandt.bookseeker.data.remote.domain.Book
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(tableName = "BookDB")
@TypeConverters(DateConverter::class)
data class BookDB(
    @PrimaryKey val artistId: Int = 0,
    @ColumnInfo(name = "artistIds") val artistIds: List<Int>? = null,
    @ColumnInfo(name = "artistName") val artistName: String? = null,
    @ColumnInfo(name = "artistViewUrl") val artistViewUrl: String? = null,
    @ColumnInfo(name = "artworkUrl100") val artworkUrl100: String? = null,
    @ColumnInfo(name = "artworkUrl60") val artworkUrl60: String? = null,
    @ColumnInfo(name = "currency") val currency: String? = null,
    @ColumnInfo(name = "description") val description: String? = null,
    @ColumnInfo(name = "fileSizeBytes") val fileSizeBytes: Int?  = null,
    @ColumnInfo(name = "formattedPrice") val formattedPrice: String? = null,
    @ColumnInfo(name = "genreIds") val genreIds: List<String>?  = null,
    @ColumnInfo(name = "genres") val genres: List<String>?  = null,
    @ColumnInfo(name = "kind") val kind: String? = null,
    @ColumnInfo(name = "price") val price: Double? = null,
    @ColumnInfo(name = "releaseDate") val releaseDate: String? = null,
    @ColumnInfo(name = "trackCensoredName") val trackCensoredName: String? = null,

    @ColumnInfo(name = "trackId") val trackId: Int? = null,
    @ColumnInfo(name = "trackName") val trackName: String? = null,
    @ColumnInfo(name = "trackViewUrl") val trackViewUrl: String? = null,
    @ColumnInfo(name = "created") val created: Date?
): Parcelable {
    companion object {
        fun to(book: Book): BookDB {
            return BookDB(
                artistId = book.artistId,
                artistIds = book.artistIds,
                artistName = book.artistName,
                artistViewUrl = book.artistViewUrl,
                artworkUrl100 = book.artworkUrl100,
                artworkUrl60 = book.artworkUrl60,
                currency = book.currency,
                description = book.description,
                fileSizeBytes = book.fileSizeBytes,
                formattedPrice = book.formattedPrice,
                genreIds = book.genreIds,
                genres = book.genres,
                kind = book.kind,
                price = book.price,
                releaseDate = book.releaseDate,
                trackCensoredName = book.trackCensoredName,
                trackId = book.trackId,
                trackName = book.trackName,
                trackViewUrl = book.trackViewUrl,
                created = Date()
            )
        }
    }
}