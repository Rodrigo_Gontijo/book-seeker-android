package com.ciandt.bookseeker.ui.search

import android.view.View
import android.widget.Toast
import com.ciandt.bookseeker.core.BaseViewModel
import com.ciandt.bookseeker.data.db.dao.BookmarkDao
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.data.remote.api.SearchAPI
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.util.NotNullMutableLiveData
import com.ciandt.bookseeker.util.ioThread
import com.ciandt.bookseeker.extension.with
import io.reactivex.subjects.PublishSubject


class SearchViewModel(private val api: SearchAPI, private val dao: BookmarkDao) : BaseViewModel() {


    private var query: String = ""
        get() = if (field.isEmpty()) "MVVM" else field

    private val _refreshing: NotNullMutableLiveData<Boolean> = NotNullMutableLiveData(false)
    val refreshing: NotNullMutableLiveData<Boolean>
        get() = _refreshing

    private val _items: NotNullMutableLiveData<List<Book>> = NotNullMutableLiveData(arrayListOf())
    val items: NotNullMutableLiveData<List<Book>>
        get() = _items

    fun doSearch() {
        val params = mutableMapOf<String, String>().apply {
            this["term"] = query
            this["entity"] = "ibook"
        }

        addToDisposable(api.search(params).with()
            .doOnSubscribe { _refreshing.value = true }
            .doOnSuccess { _refreshing.value = false }
            .doOnError { _refreshing.value = false }
            .subscribe({
                _items.value = it.results
            }, {
                // handle errors
            }))
    }

    fun onQueryChange(query: CharSequence) {
        this.query = query.toString()
    }

    fun saveToBookmark(repository: Book) = ioThread {
        dao.insert(BookDB.to(repository))

    }
}