package com.ciandt.bookseeker

import com.ciandt.bookseeker.data.remote.api.SearchAPI
import com.ciandt.bookseeker.data.remote.response.BookList
import io.reactivex.Single


class DummySearchAPI : SearchAPI {

    override fun search(params: MutableMap<String, String>): Single<BookList> {
        return Single.just(null)
    }

}