package com.ciandt.bookseeker.ui.bookdetails

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("app:image")
fun setImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}