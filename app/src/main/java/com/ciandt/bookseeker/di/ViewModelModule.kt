package com.ciandt.bookseeker.di

import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.ui.bookdetails.BookDetailsViewModel
import com.ciandt.bookseeker.ui.bookmark.BookmarkViewModel
import com.ciandt.bookseeker.ui.search.SearchViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


val viewModelModule = module {
    viewModel { SearchViewModel(get(), get()) }
    viewModel { BookmarkViewModel(get()) }
    viewModel { BookDetailsViewModel() }
}