package com.ciandt.bookseeker.ui.bookmark

import android.content.Intent
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.ui.bookdetails.BookDetailsActivity
import com.squareup.picasso.Picasso


@BindingAdapter(value = ["bookmarks", "viewModel"])
fun setBookmarks(view: RecyclerView, items: PagedList<BookDB>?, vm: BookmarkViewModel) {
    view.adapter?.run {
        if (this is BookmarkAdapter)
            this.submitList(items)
    } ?: run {
        BookmarkAdapter(vm).apply {
            view.adapter = this
            this.submitList(items)
        }
    }
}

@BindingAdapter("app:image")
fun setImage(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}

@BindingAdapter("app:onClick")
fun setOnClickListener(view: View, book: BookDB) {
    view.setOnClickListener {
        val intent = Intent(view.context, BookDetailsActivity::class.java)
        intent.putExtra("BookDB", book)
        view.context.startActivity(intent)
    }
}