package com.ciandt.bookseeker.di

import com.ciandt.bookseeker.data.remote.api.SearchAPI
import org.koin.dsl.module.module
import retrofit2.Retrofit


val apiModule = module {
    single(createOnStart = false) { get<Retrofit>().create(SearchAPI::class.java) }
}