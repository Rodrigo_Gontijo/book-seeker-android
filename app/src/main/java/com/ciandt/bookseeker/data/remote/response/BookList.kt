package com.ciandt.bookseeker.data.remote.response

import com.ciandt.bookseeker.data.remote.domain.Book
import com.google.gson.annotations.SerializedName


data class BookList(
    @SerializedName("resultCount")
    val resultCount: Int,
    @SerializedName("results")
    val results: List<Book>
)