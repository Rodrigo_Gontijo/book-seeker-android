package com.ciandt.bookseeker.ui.bookmark

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ciandt.bookseeker.core.BaseViewModel
import com.ciandt.bookseeker.data.db.dao.BookmarkDao
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.util.ioThread


class BookmarkViewModel(private val dao: BookmarkDao) : BaseViewModel() {
    val items: LiveData<PagedList<BookDB>> = LivePagedListBuilder(dao.findAll(),  /* page size */ 10).build()

    fun delete(bookmark: BookDB) = ioThread { dao.delete(bookmark) }




}