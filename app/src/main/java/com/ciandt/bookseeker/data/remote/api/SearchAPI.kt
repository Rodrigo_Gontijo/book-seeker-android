package com.ciandt.bookseeker.data.remote.api

import com.ciandt.bookseeker.data.remote.response.BookList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface SearchAPI {

    @GET("search?")
    fun search(@QueryMap params: MutableMap<String, String>): Single<BookList>

}