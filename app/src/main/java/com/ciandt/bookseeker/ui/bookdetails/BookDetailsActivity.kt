package com.ciandt.bookseeker.ui.bookdetails

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import com.ciandt.bookseeker.R
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.databinding.ActivityBookDetailsBinding
import com.ciandt.bookseeker.databinding.ActivityBookmarkBinding
import com.ciandt.bookseeker.ui.BindingActivity
import org.koin.android.ext.android.setProperty
import org.koin.androidx.viewmodel.ext.android.getViewModel


class BookDetailsActivity : BindingActivity<ActivityBookDetailsBinding>() {


    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_book_details

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val objectDB = intent.extras.get("BookDB")
        val objectRepository  = intent.extras.get("BookRepository")

        val myViewModel: BookDetailsViewModel = getViewModel()

        if(objectDB != null) {
            val copyObjectDV = objectDB as BookDB
            myViewModel.setUrl(copyObjectDV.artworkUrl100)
            myViewModel.setDescription(copyObjectDV.description)
            myViewModel.setName(copyObjectDV.trackName)
            myViewModel.setAuthor(copyObjectDV.artistName)
        }
        else {

            val copyObjectDV = objectRepository as Book
            myViewModel.setUrl(copyObjectDV.artworkUrl100)
            myViewModel.setDescription(copyObjectDV.description)
            myViewModel.setName(copyObjectDV.trackName)
            myViewModel.setAuthor(copyObjectDV.artistName)
        }






        binding.vm = myViewModel
        binding.setLifecycleOwner(this)




    }
}