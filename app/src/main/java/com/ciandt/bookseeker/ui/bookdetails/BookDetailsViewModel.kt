package com.ciandt.bookseeker.ui.bookdetails

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.ciandt.bookseeker.core.BaseViewModel
import com.ciandt.bookseeker.data.db.dao.BookmarkDao
import com.ciandt.bookseeker.data.db.entity.BookDB
import com.ciandt.bookseeker.data.remote.domain.Book
import com.ciandt.bookseeker.util.ioThread
import com.squareup.picasso.Picasso

class BookDetailsViewModel() : BaseViewModel() {

    var nameBook : String? = ""
    var urlBook : String? = ""
    var descriptionBook : String? = ""
    var authorBook : String? = ""

    fun setName(item : String?){
        this.nameBook = item
    }

    fun setUrl(item : String?){
        this.urlBook = item
    }

    fun setDescription(item : String?){
        this.descriptionBook = item
    }

    fun setAuthor(item : String?){
        this.authorBook = item
    }


}